#include <stdio.h>

int main()
{
  int num[10];
  for(int i = 0; i < 10; ++i)
  {
     num[i]=500;
  }

  printf("Displaying the values ");

  for(int i = 0; i < 10; ++i)
  {
     printf("%d\n",num[i]);
  }
  return 0;
}