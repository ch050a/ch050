#include <stdio.h>
int binarySearch(int a[], int l, int h, int x) 
{ 
    while (l <= h) { 
        int m = l + (h - l) / 2; 
    if (a[m] == x) 
            return m; 
    if (a[m] < x) 
            l = m + 1; 
    else
            h = m - 1; 
    } 
    return -1; 
} 
  
int main(void) 
{ 
    int a[] = { 1,2,3,4,5,6,7,8,9,10}; 
    int n = sizeof(a) / sizeof(a[0]); 
    for (int i = 0; i < n; i++) 
        printf("%d ",a[i]); 
	int x;
	printf("\nEnter the Element\n");
    scanf("%d", &x);
    int result = binarySearch(a, 0, n - 1, x); 
    if (result == -1)
		printf("Not found");
	else
		printf("Element is found at index %d",result );
	return 0; 
} 