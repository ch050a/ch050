#include <stdio.h>

int main()
{
  int n, r = 0, s;

  printf("Enter a number to check if it's a palindrome or not");
  scanf("%d", &n);
  s = n;

  while (s != 0)
  {
    r = r * 10;
    r = r + s%10;
    s = s/10;
  }

  if (n == r)
    printf("%d is a palindrome number.\n", n);
  else
    printf("%d isn't a palindrome number.\n", n);

  return 0;
}