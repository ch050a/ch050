#include <stdio.h>

int main()
{
    int limit, num, pos = 0, neg = 0, zero = 0;
    do
    {
        printf("Enter the number");
        scanf("%d", &num);

        if(num > 0)
        {
            pos++;
        }
        else if(num < 0)
        {
            neg++;
        }
        else
        {
            zero++;
        }

        
    }while(num!=-1);

    printf("\nPositive Numbers: %d\n", pos);
    printf("Negative Numbers: %d\n", neg);
    printf("Number of zero: %d\n", zero);

    return 0;
}