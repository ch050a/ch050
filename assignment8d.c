#include <stdio.h>
int main()
{
     float marks[10], sum = 0, average;
    int n;
     printf("Enter number of elements: ");
     scanf("%d", &n);

     for(int i=0; i<n; ++i)
     {
          printf("Enter number %d: ",i+1);
          scanf("%f", &marks[i]);
          
          sum += marks[i];
     }

     average = sum/n;
     printf("Average = %f", average);

     return 0;
}