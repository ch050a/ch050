#include<stdio.h>
#include<math.h>
void Roots(float a, float b, float c)
{
    if (a == 0)
    {
        printf("Invalid");
        return;
    }
 
    float d = b*b - 4*a*c;
    float sqr = sqrt(d);
 
    if (d > 0)
    {
        printf("Roots are real and different \n");
        printf("%f\n%f",(-b + sqr)/(2*a)
            , (-b - sqr)/(2*a));
    }
    else if (d == 0)
    {
        printf("Roots are real and same \n");
        printf("%f",-b / (2*a));
    }
    else
    {
        printf("Roots are complex \n");
    }
}
void main()
{
    float a, b, c;
    printf("Enter the coefficients of the quadratic equation a, b and c:  ");
    scanf("%f%f%f",&a,&b,&c);
    Roots(a, b, c);
}